//en.js
const en = {
    message: {
        'langs': "中文",
        'hello': 'hello, world',
        'hi': 'hi man'
    },
    //主页导航
    nav: {
        'corporateoveriew': "CORPORATE",
        'productsandservices': 'PRODUCTS',
        'networks': "NETWORKS",
        'globapartners': 'PARTNERS',
        'contact': 'CONTACT'
    },
    serverName:{
        'ssxt':"Comfort Solution",
        'ktxt':"Climate Solution",
        'dkxt':"Electronics Solution",
        'fzcp':"ancillary product"
    },
    //产品分类
    category: {
        'climatesolution': "Climate Solution",
        "comfortsolution": "Comfort Solution",
        "electronicsSolution": "Electronics Solution",
        "ancillaryproduct": "ancillary product"
    },
    //企业介绍导航
    childnav: {
        "0": "About Us",
        "1": "Our History",
        "2": "Our Advantages"
    },
    //产品布局
    networks: {
        'domestic': "DOMESTIC",
        'info': 'We have valued customers in more than 20 provinces and cities nationwide. More than 90 customers across the country, including the clients listed below.',
        'international': "INTERNATIONAL",
        'info1': 'India, Thailand, Europe, North America, South Africa and more.',
        'info2': "Our 6 production bases and 29 logistics warehouses span across the nation from the north to south and are rooted in more than 20 provinces and cities in China. Our focus is on providing customers with convenient, high-quality and localised services."
    },
    //我们的客户
    ourclients: {
        "info": "Quality-oriented, deeply innovative new product & technology, and a focus on customer service give us unparalleled core competitiveness. After nearly 30 years of succeed in our business, we have built a comprehensive network of core customers around the globe. We have established long-term and effective strategic partnerships with more than 90 companies around the world. In the near future, New Development will provide more companies with complete intelligent seat comfort solutions. Your success is our success!"
    },
    //联系我们
    contact: {
        "gs": "Changchun New Development Of Plastic Industry Co.,Ltd",
        "tel": "TEL:0431-89851760",
        "email": "Email:ccxfz@ccxfz.com",
        'add': "Address:1501,No.2221,Dongnanhu Road,Economic District,Changchun,China"
    },
    //企业介绍数据
    //关于我们
    aboutme: {
        'whoareyou': 'WHO WE ARE?',
        'whoareyouinfo': 'Established in 1988, our company is now recognised as Changchun New Development Automotive Systems Co. Ltd and headquartered in Changchun, the cradle of China’s automobile industry. Our founder came from China’s  rst automobile manufacturer - FAW Group currently employs over 300 exceptional staff, with hundreds specialising in engineering and technical management. A good proportion of the upper management personnel has worked for the likes of Adient, Lear, Brose, Faurecia and other world-leading automotive seating specialists, in the world’s top 500 companies.',
        'whatwedeliver': 'WHAT WE DELIVER?',
        'whatwedeliverinfo': 'Our mission is to provide automotive seating manufacturers with intelligent core parts and complete solutions. We are a market-leading company supplying seat parts and a variety of products to the automotive industry, servicing over 20 million cars annually. Our equipment includes Engel, Demag, Haitian in injection moulding, Wa os, Vinston in steel wire forming, and ZEISS, ASML in research. We also utilise more than 300 advanced national and international facilities and 19 motor production lines with assembly lines located in the 6 major production bases across the country. A network layout of 29 secondary distribution centres ensures our customers always receive timely delivery and rapid response with quality service.',
        'howwedelicer': 'HOW WE DELIVER?',
        'howwedelicerinfo': 'map and move toward becoming an iconic, sustainable, international business bringing high quality services and products to a new era. Innovation starts here, development continues.',
        'whatisourfuture': 'WHAT IS OUR FUTURE?',
        'whatisourfutureinfo': 'We apply the vision of cloud thinking to target the world map and move toward becoming an iconic, sustainable, international business bringing high quality services and products to a new era. Innovation starts here, development continues.'
    },
        //发展历程
        fzlc:{
            1988:"Established Changchun Nanguan District Culture & Education Plastic Factory",
            198801:"Joined Jilin Yatai, Jilin Construction Engineering Group supply system",
            1993:"Established Changchun Nanguan District Automobile Safety Belt Factory",
            199301:"Joined FAW Group supply system",
            1999:"Established China Changchun New Development Plastic Industry Co. Ltd.",
            199901:"Entered Changchun Xuyang Faway Johnson Automotive Seat Frame Co. Ltd supply system ",
            2003:"Entered Shenyang Jinbei Adient, Faway Mekera Lang supply system",
            2007:"Entered Chongqing Yanfeng Adient supply system",
            200701:"Entered Xuyang Faurecia supply system",
            200702:"Entered Great Wall Motors supply system",
            200703:"Entered Changchun Faway Adient supply system",
            2011:"Entered Shanghai Yanfeng Adient supply system",
            201101:"Entered Shanghai Intier Jiao Yun supply system ",
            201102:"Entered BYD Auto supply system",
            201103:"Established Kunshan Factory",
            2013:"Entered Brose (China) supply system ",
            201301:"Became Lear Global Supplier",
            201302:"Entered Roechling supply system",
            201303:"Established of Chongqing Factory",
            201304:"Innovated and developed customised lumbar support parts with FAW-Volkswagen",
            2015:"Entered Changan Ford supply system",
            201501:"Entered Yantai Yanfeng Adient supply system",
            201502:"Established of Shenyang South Factory, Shenyang North Factory, Yantai Factory and Harbin Factory",
            2016:"  Established of Hangzhou Factory",
            201601:"Entered Geely Volvo supply system",
            2017:"Established of Dongguan Factory",
            201701:"Entered Wuhan DMC Lear supply system"
        },
    //企业优势
    advantages: {
        'whychooseus': "WHY CHOOSE US?",
        'whychooseusinfo': "We are currently the world’s only complete intelligent comfort seat solution provider servicing automotive manufacturers, covering three solutions and 10 core product series. Our customers are high-pro le automotive manufacturers and our developed products have all passed industry experiment standards and requirements for various countries.Our products have served the industry for more than 30 years and have given us a market-leading reputation. The products we supply have an absolute advantage in cost performance, and our customers are our greatest testament to the quality of our work as we continue to see many referrals and repeat business.",
        "information": "INFORMATION MANAGEMENT",
        "informationinfo": "Since 2013, our goals of building smart digital companies, ERP systems, OA systems, and DSS systems have been introduced one after another to create a cutting-edge company for precision production and timely delivery. In the near future we will introduce PIS and PLM systems to further enhance our project management and product life cycle management capabilities.",
        'technological': "TECHNOLOGICAL INNOVATION",
        'technologicalinfo': "Taking design innovation and management expertise as a starting point, we drive technical advancement. Over the years we have introduced, developed and established our 105 patents with independent intellectual property rights. We have also obtained more than RMB 500 million in scienti c and technological research funds from state, province, city and district levels. Since 2013, the company has designed and developed speci c seat comfort systems simultaneously with its manufacturing process in collaboration with FAW-Volkswagen automotive manufacturers, and has been highly praised by our partners.",
        'visionofoperation': "VISION OF OPERATION",
        "visionofoperationinfo": "Market-oriented, centralised & customer-focused.",
        'qualitypolicy': "QUALITY POLICY",
        "qualitypolicyinfo": "Continuous improvement beyond customers’ growth, demands and expectations. "
    },
    //座椅空调系统
    climateSolution: [{
        'id': '01',
        'name': 'Heated Series',
        'msg': 'Resistance Wire Heating Pad',
        'info': 'Whether in the cold of winter or the heat of summer, comfortable seats that match the body temperature give passengers a beautiful driving experience. Our products use advanced carbon  bre material, a unique mesh full-cover design, and controlled seat surface temperature to provide a superior level of comfort.',
        'pic': "./dist/swiperData/tongfengjiare.png"
    }, {
        'id': '02',
        'name': 'Ventilated Series',
        'msg': 'Blowing & Suction Equipmen',
        'info': 'In the hot summer environment, the accumulation of perspiration can greatly reduce driving pleasure. Our products are equipped with ventilated air pumps on the backrest and cushions to accelerate air circulation to passengers to take away human perspiration, making the ride supremely pleasant and enjoyable.',
        'pic': "./dist/swiperData/tongfeng.png"
    }, {
        'id': '03',
        'name': 'Ventilated Series',
        'msg': 'Blowing & Suction Equipmen',
        'info': 'In the hot summer environment, the accumulation of perspiration can greatly reduce driving pleasure. Our products are equipped with ventilated air pumps on the backrest and cushions to accelerate air circulation to passengers to take away human perspiration, making the ride supremely pleasant and enjoyable.',
        'pic': "./dist/swiperData/tongfeng2.png"
    }, {
        'id': '04',
        'name': 'Air-Conditioned Series',
        'msg': 'Air-Conditioner',
        'info': 'In order to allow seats to rapidly reach an optimum temperature, we have set up an overall control system that independently sends air to the seat surface. This enables a multi-pronged approach to quickly adjust seat surface temperature, making the journey pleasant and comfortable.',
        'pic': "./dist/swiperData/kongtiaoxilie1.png"
    }, {
        'id': '05',
        'name': 'Air-Conditioned Series',
        'msg': 'Air-Conditioner',
        'info': 'In order to allow seats to rapidly reach an optimum temperature, we have set up an overall control system that independently sends air to the seat surface. This enables a multi-pronged approach to quickly adjust seat surface temperature, making the journey pleasant and comfortable.',
        'pic': "./dist/swiperData/kongtiaoxilie2.png"
    }],
    //座椅舒适系统
    comfortSolution: [{
        'id': '06',
        'name': 'Lumbar Support Series',
        'msg': 'Pallet Manual Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng1.png'
    }, {
        'id': '07',
        'name': 'Lumbar Support Series',
        'msg': 'Braced Manual Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng2.png'
    }, {
        'id': '08',
        'name': 'Lumbar Support Series',
        'msg': 'Grid Manual Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng3.png'
    }, {
        'id': '09',
        'name': 'Lumbar Support Series',
        'msg': 'Two-way Pneumatic Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng4.png'
    }, {
        'id': '010',
        'name': 'Lumbar Support Series',
        'msg': 'Two-way Pneumatic Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng5.png'
    }, {
        'id': '011',
        'name': 'Lumbar Support Series',
        'msg': 'Four-way Pneumatic Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng6.png'
    }, {
        'id': '012',
        'name': 'Lumbar Support Series',
        'msg': 'Pallet Electric Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng7.png'
    }, {
        'id': '013',
        'name': 'Lumbar Support Series',
        'msg': 'Grid Electric Lumbar Support',
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': './dist/swiperData/yaobuzhicheng8.png'
    }, {
        'id': '015',
        'name': 'Cushion Suspension Series',
        'msg': 'Injection-moulded CushionSuspension',
        'info': 'Bumpy roads can have an adverse effect on the comfort of passengers and a fatigued journey will greatly impact the driving experience. By using advanced engineering & technologies with the installation of our cushion suspension system, we can achieve a “soft landing” for the passengers and give an enjoyable and smooth driving experience.',
        'pic': './dist/swiperData/zuoyixuangua1.png'
    }, {
        'id': '016',
        'name': 'Cushion Suspension Series',
        'msg': 'Clipped Cushion Suspension',
        'info': 'Bumpy roads can have an adverse effect on the comfort of passengers and a fatigued journey will greatly impact the driving experience. By using advanced engineering & technologies with the installation of our cushion suspension system, we can achieve a “soft landing” for the passengers and give an enjoyable and smooth driving experience.',
        'pic': './dist/swiperData/zuoyixuangua2.png'
    }, {
        'id': '017',
        'name': 'Backrest Suspension Series',
        'msg': 'T-shape Mesh + Supporting Wire',
        'info': 'The back-cushioning device is the best option to further enhance the experience of a bumpy journey. Backrest suspension series products provide full elastic support for passengers, allowing the back to be fully relaxed and in tune with the rhythm of driving.',
        'pic': './dist/swiperData/kaobeixuangua1.png'
    }, {
        'id': '018',
        'name': 'Backrest Suspension Series',
        'msg': 'T-shape Mesh Backrest Grid',
        'info': 'The back-cushioning device is the best option to further enhance the experience of a bumpy journey. Backrest suspension series products provide full elastic support for passengers, allowing the back to be fully relaxed and in tune with the rhythm of driving.',
        'pic': './dist/swiperData/kaobeixuangua2.png'
    }, {
        'id': '019',
        'name': 'Backrest Suspension Series',
        'msg': 'Straight Wire Backrest Grid',
        'info': 'The back-cushioning device is the best option to further enhance the experience of a bumpy journey. Backrest suspension series products provide full elastic support for passengers, allowing the back to be fully relaxed and in tune with the rhythm of driving.',
        'pic': './dist/swiperData/kaobeixuangua3.png'
    }],
    //座椅电控系统
    ElectronicsSolution: [{
        'id': '020',
        'name': 'Seat Adjustment Motor Series',
        'msg': " Angle Adjustment Motor",
        'info': 'The matching of seat shape and passenger’s body shape makes driving comfortable and safe. Our seat adjustment products offer  ngertip touch and easy seat control,  nding the optimum driving posture. Easy operation leads to a safer and more comfortable driving experience.',
        'pic': './dist/swiperData/diankongxitong1.png'
    }, {
        'id': '021',
        'name': 'Seat Adjustment Motor Series',
        'msg': "Rail Adjustment Motor",
        'info': 'The matching of seat shape and passenger’s body shape makes driving comfortable and safe. Our seat adjustment products offer  ngertip touch and easy seat control,  nding the optimum driving posture. Easy operation leads to a safer and more comfortable driving experience.',
        'pic': './dist/swiperData/diankongxitong2.png'
    }, {
        'id': '022',
        'name': 'Seat Adjustment Motor Series',
        'msg': "Elevating Motor",
        'info': 'The matching of seat shape and passenger’s body shape makes driving comfortable and safe. Our seat adjustment products offer  ngertip touch and easy seat control,  nding the optimum driving posture. Easy operation leads to a safer and more comfortable driving experience.',
        'pic': "./dist/swiperData/diankongxitong3.png"
    }, {
        'id': '023',
        'name': 'Adjustable Six-way Headrest Series',
        'msg': "",
        'info': 'Head and neck comfort are essential for safe seat design. Safety and comfort are the key focuses of our headrest products. The simple  nger touch control can easily adjust the position of the headrest making the journey comfortable and safe.',
        'pic': "./dist/swiperData/maozhan.png"
    }, {
        'id': '024',
        'name': 'Seat Belt Sensor Series',
        'msg': "",
        'info': 'Full protection of passengers is top priority with our intelligent and comfortable seat solutions. The seatbelt sensor series of products passes through information and promptly noti es passengers to wear seatbelts immediately after the vehicle is started. This provides a safety guarantee for travel at all times.',
        'pic': "./dist/swiperData/A1.png"
    }, {
        'id': '025',
        'name': 'Seat Belt Sensor Series',
        'msg': "",
        'info': 'Full protection of passengers is top priority with our intelligent and comfortable seat solutions. The seatbelt sensor series of products passes through information and promptly noti es passengers to wear seatbelts immediately after the vehicle is started. This provides a safety guarantee for travel at all times.',
        'pic': "./dist/swiperData/B1.png"
    }, {
        'id': '026',
        'name': 'Seat Belt Sensor Series',
        'msg': "",
        'info': 'Full protection of passengers is top priority with our intelligent and comfortable seat solutions. The seatbelt sensor series of products passes through information and promptly noti es passengers to wear seatbelts immediately after the vehicle is started. This provides a safety guarantee for travel at all times.',
        'pic': "./dist/swiperData/B2.png"
    }, {
        'id': '028',
        'name': 'Intelligent-Seat Control Applicaiton',
        'msg': "",
        'info': 'Smart phone devices and mobile technology continually give us new product development ideas. We have integrated seat control with a mobile Application, eliminating the need for handheld keys or manual adjustment processes. Simply tapping the screen will complete an intelligent remote control. The comfort experience has never been so easy.',
        'pic': "./dist/swiperData/phoneapp.png"

    }],
    ancillaryProduct: [{
        'id': '029',
        'name': 'Plastic Parts Series',
        'msg': " Door Plastic Parts",
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': "./dist/swiperData/suliaopeijian1.png"
    }, {
        'id': '030',
        'name': 'Plastic Parts Series',
        'msg': " Seat Plastic Parts",
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': "./dist/swiperData/suliaopeijian2.png"
    }, {
        'id': '031',
        'name': 'Plastic Parts Series',
        'msg': "Headrest Plastic Parts",
        'info': 'In the process of driving, waist muscles can tense up and cause fatigue and discomfort. In order to achieve a comfortable experience, we apply lean technology based on ergonomic principles to deliver lumbar support that meets the requirements of various models. This promotes blood circulation to enhance well-being and relieve fatigue of passengers, providing a relaxing and enjoyable driving experience.',
        'pic': "./dist/swiperData/suliaopeijian3.png"
    }, {
        'id': '032',
        'name': 'Steel Wire Series',
        'msg': "Foaming Wire",
        'info': 'Advance and precise production is our promise for our industrialproducts. New development using advance wire forming equipment made by Wa os, Vinston and other  nely de ne',
        'pic': "./dist/swiperData/10.png"
    }, {
        'id': '033',
        'name': 'Steel Wire Series',
        'msg': "Rear Seat Welding Wire",
        'info': 'Advance and precise production is our promise for our industrialproducts. New development using advance wire forming equipment made by Wa os, Vinston and other  nely de ne',
        'pic': "./dist/swiperData/13.png"
    }, {
        'id': '034',
        'name': 'Steel Wire Series',
        'msg': "Wing Welding Wire",
        'info': 'Advance and precise production is our promise for our industrialproducts. New development using advance wire forming equipment made by Wa os, Vinston and other  nely de ne',
        'pic': "./dist/swiperData/hjgs.png"
    }, {
        'id': '035',
        'name': 'Spring Series',
        'msg': "",
        'info': 'High quality Cushioning equipment is inseparable from durable spring components, accurately moulded and precise quality controlled spring products allows the next level comfort in seating.',
        'pic': "./dist/swiperData/tanhuang.png"
    }, {
        'id': '036',
        'msg': "",
        'name': 'Seat Map Pocket with Nylon Mesh',
        'info': 'High quality Cushioning equipment is inseparable Stability of baggage items greatly in uences the safety and comfort of driving. The use of high- quality nylon materials allows for both lightness and strength. On the basis of aesthetics and durability, the nylon mesh series of products secures luggage, eliminating the possibility of bouncing and accidents.',
        'pic': "./dist/swiperData/nilongwang.png"
    }, {
        'id': '037',
        'msg': "",
        'name': 'Seat Unlock Cable Series',
        'info': 'Using advanced moulding equipment, our seat unlock cable perfectly  ts the internal structure of the seat. On top of that, the high-standard of material and precise processing technology make the product more durable, giving passengers a smooth operation of the vehicle.',
        'pic': "./dist/swiperData/zuoyijiesuolaxian.png"
    }, {
        'id': '038',
        'msg': "",
        'name': 'Seat Joystick Series',
        'info': 'Using advanced alloy materials, we strive to ensure accuracy in product dimensions, while advanced plastic injection moulding processes make products more ergonomic. This maintains durability and comfort, allowing riders to enjoy a silky-smooth seat adjustment experience.',
        'pic': "./dist/swiperData/zuoyicaozonggan.jpg"
    }, {
        'id': '040',
        'msg': "",
        'name': 'Felt Series',
        'info': 'Bumps and noises can be unavoidable while driving and durable and  rm felt fabric can effectively reduce this issue. Our precise cutting process allows felt products to  t snuggly onto the seat to cushion bumps, soften noises, and allow drivers and passengers to enjoy a leisurely journey.',
        'pic': "./dist/swiperData/18.png"
    }]

}

export default en
