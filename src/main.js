import Vue from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store/store";
import './assets/base.css';
import i18n from './i18n/i18n';
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import './assets/rem.js';
import './assets/font/iconfont.css';

import VueAwesomeSwiper from "vue-awesome-swiper";
// require styles
import "swiper/dist/css/swiper.css";
Vue.use(VueAwesomeSwiper);
Vue.use(MintUI)
new Vue({
    el: '#app',
    router,
    store,
    i18n,
    render: h => h(App)
})