import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {

        productid: 1
    },

    actions: {
        //可以是异步操作
        ADD_DATA: (store, data) => {
            store.commit("ADD_DATA_MUTATION", data);
        }
    },

    mutations: {
        //必须是同步操作
        ADD_DATA_MUTATION: (state, data) => {
            state.productid = data.product_id
        }
    },

    getters: {
        ProcessedShopData(state) {

            //处理数据(将相同商家的产品进行归类， 给shopcar页面输出)
            let merchants = [];

            state.shopcarData.forEach(item => {

                if (!merchants.some(merchant => merchant.id == item.merchant_id)) {

                    merchants.push({ id: item.merchant_id, name: item.merchant_name, isChecked: false });
                }
            })

            merchants.forEach(merchant => {
                merchant.children = state.shopcarData.filter(item => item.merchant_id == merchant.id);

                //判读是否子元素全部选中
                merchant.children.every(item => item.isChecked == true) ? merchant.isChecked = true : merchant.isChecked = false;
            })

            return merchants;
        }
    }

})

export default store;