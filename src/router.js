import Vue from 'vue';
import VueRouter from 'vue-router';
// import Root from "./components/Root.vue";
// import home from "./components/home.vue";
// import Introduce from "./components/Introduce.vue" //企业介绍
// import ProductService from "./components/ProductService.vue" //产品服务
// import ProductLayout from "./components/ProductLayout.vue" //产品布局
// import ServiceNetwork from "./components/ServiceNetwork.vue" //业务网络
// import Contact from "./components/Contact.vue" //联系方式

Vue.use(VueRouter);
VueRouter.prototype.goBack = function () {
    this.isBack = true
    console.log('123')
    window.history.go(-1)
}
const routes = [{
    path: "/introduce",
    name: "introduce",
    component: (resolve) => {
        require(['./components/Introduce.vue'], resolve)
    },
    // component: Introduce //要独立于root之外， 不属于root的子路由
},
{
    path: '/',

    redirect: "/home",
    component: (resolve) => {
        require(['./components/Root.vue'], resolve)
    },
    children: [
        {
            path: "/home",
            name: "home",
            component: (resolve) => {
                require(['./components/home.vue'], resolve)
            }
            // component: home
        }
    ]
    // component: Root,
},
{
    path: "/productService/:id?",
    name: "productService",
    props: true,
    component: (resolve) => {
        require(['./components/productService.vue'], resolve)
    }
    // component: ProductService
},
{
    path: "/ProductLayout",
    name: "productLayout",
    component: (resolve) => {
        require(['./components/ProductLayout.vue'], resolve)
    }
    // component: ProductLayout
}, {
    path: "/ServiceNetwork",
    name: "servicenetwork",
    component: (resolve) => {
        require(['./components/servicenetwork.vue'], resolve)
    }
    // component: ServiceNetwork
},
{
    path: "/Contact",
    name: "contact",
    component: (resolve) => {
        require(['./components/Contact.vue'], resolve)
    }
    // component: Contact
}
]


const router = new VueRouter({
    routes,
    // mode: 'history',

}) //加载所有模块(组件是按需加载)

export default router;